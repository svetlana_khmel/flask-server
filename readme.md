These repos are the same demo-projects:

* 1) Crome extention gets current url, and referer and sends them to the node server.
Then waits for the category name and shows the icon according to the category.
https://bitbucket.org/svetlana_khmel/bear-chrome-exstention/src/master/

* 2) Node server gets links from the chrome extention, sends link to the python server, 
wait from the python server a response with category and sends the category to the 
chrome extention.
https://bitbucket.org/svetlana_khmel/plain-server-get-json/src/master/

* 3) Python server gets link from node server and sends back to the node server.
https://bitbucket.org/svetlana_khmel/flask-server/src/master/




---------------------------------

1) To create an image, create Dockerfile (configurale file)

- To build an image (configuration - Dockerfile):
docker build -t python-server .

- To see images
docker images

- Run a container from that image
 docker run -p 5000:5000 python-server

- Build the container (Instance of image, file - docker-compose.yml)
docker-compose build

- Start all the containers:
docker-compose up

-Stop all the containers
Docker-compose stop

- See running images
docker ps
docker container ls

—> Better to start separately
docker-compose up -d mongo
docker-compose up -d app
docker-compose up -d client


-Delete image
docker rmi c32705f7fde6

Docker ——> documentation

docker-compose up —>starts all the containers
docker-compose up -d mongo —>we are gonna run mongo first

docker ps ——> Checks if the container started

docker-compose up -d app



- How to share docker images without Docker hub or any registry
https://medium.com/bb-tutorials-and-thoughts/how-to-share-docker-images-without-docker-hub-or-any-registry-6cb7aade1783


- Container is the actual instantiation of the image just like how an object is an instantiation or an instance of a class. A Docker image packs up the application and environment required by the application to run, and a container is a running instance of the image.






-Print the output of your app:
# Get container ID
$ docker ps
# Print app output
$ docker logs <container id>


python3 -m venv niceproject cd niceproject source niceproject/bin/activate
deactivate



https://flask-sqlalchemy.palletsprojects.com/en/2.x/quickstart/

-----------------------------------------------------
Prerequisites for installing Python3 on Mac
Install Xcode
Xcode is Apple's Integrated Development Environment (IDE). You might already have Xcode on your Mac. If not, you can get Xcode from Apple appstore.

Xcode
Install Brew
Homebrew installs the stuff you need. Homebrew is a package manager for Mac OS

Step 1. Launch Terminal.

Go to Launchpad – Other – Terminal

Step 2. Install HomeBrew


/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

Install Python3 with Brew
Enter brew command into terminal

brew install python3
Optional, PATH environment
Set up PATH environment variable, if you used HomeBrew to install Python3, then HomeBrew already added PATH.

Do not change PATH environment if you can launch python3 from terminal.

Add the following line to your ~/.profile file

export PATH=/usr/local/bin:/usr/local/sbin:$PATH
Usually your Python installation directory looks like this, add it to your PATH

PATH="/Library/Frameworks/Python.framework/Versions/3.6/bin:${PATH}"
-------------------------------------------------

Db.SQLAlchemy(app)

Class Person(db.Model):
id=db.Column(db.Intenger, primary_key=True)
name = db.relationship(‘Pet’, backhref=‘owner’)

class Pet(db.Model):
id = db.Column(db.Intenger, primary_key=True)
name = db.Column(db.String(20))
owner_id = db.Column(db.Intenger, db.ForeignKey(‘person.id’))

Person.query.filter_by(name=‘Anthony’).first()

———————————
python
from app import db
db.create_all()
db.drop_all()

''.join(list1)


list1 = [1, 2, 3]
str1 = ''.join(str(e) for e in list1)


Model.query.filter(Model.columnName.contains('sub_string'))
Link.query.filter_by(url=url).first()
Person.query.filter_by(name=‘Anthony’).first()

category1 = Category(name="Arts_and_Entertainment")
db.session.add(category1)
db.session.commit()

select * from table 
where name like '%BMW%'



for f in cats:
    print f
    
    
Arts_and_Entertainment  
Autos_and_Vehicles
Beauty_and_Fitness
Books_and_Literature
Business_and_Industry
Career_and_Education
Computer_and_Electronics
Finance
Food_and_Drink
Gambling
Games
Health
Home_and_Garden
Internet_and_Telecom
Law_and_Government
News_and_Media
People_and_Society
Pets_and_Animals
Recreation_and_Hobbies
Reference
Science
Shopping
Sports
Travel
Adult

Creating One-To-Many Relationships in Flask-SQLAlchemy
https://www.youtube.com/watch?v=juPQ04_twtA
https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#one-to-many


edit
play_arrow

brightness_4
# Python program to illustrate 
# while loop 
count = 0
while (count < 3):     
    count = count + 1
    print("Hello Geek") 



x = len(cars)

count = 0
while (count < x):
    count = count + 1
    