from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
from flask import request
import uuid
import json
from flask import jsonify




project_dir = os.path.dirname(os.path.abspath(__file__))
database_file = "sqlite:///{}".format(os.path.join(project_dir, "categories.db"))

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = database_file
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


# with open('sw-categories.pretty.json') as f:
#   data = json.load(f)
##############  One to many ##############
# class Category(db.Model):
#     __tablename__ = 'category'
#     name = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)
#     links = db.relationship("Link")
#
#     def __repr__(self):
#         return "<Name: {}>".format(self.name)
#
#
# class Link(db.Model):
#     __tablename__ = 'link'
#     id = db.Column(db.Integer, primary_key=True)
#     url = db.Column(db.String, unique=True)
#     category = db.Column(db.Integer, db.ForeignKey('category.name'))


class Category(db.Model):
    __tablename__ = 'category'
    name = db.Column(db.String(200), nullable=False, primary_key=True)
    links = db.Column(db.String())
    #links = db.relationship("Link")

    def __repr__(self):
        return "<Name: {}>".format(self.name)



#
# class Link(db.Model):
#     __tablename__ = 'link'
#     id = db.Column(db.Integer, primary_key=True)
#     url = db.Column(db.String, unique=True)
#     category = db.Column(db.Integer, db.ForeignKey('category.name'))


# x = len(cars)
#
# count = 0
# while (count < x):
#     count = count + 1
#
# print(x)

# f = open('sw-categories.pretty.json')
# data = json.load(f)
# f.close()

# for (k, v) in data.items():
#    #print("Key: " + k)
#    #print("Value: " + str(v))
#    list = ','.join(v)
#
#    cat = Category.query.filter_by(name=k).first()
#    print(cat)
#    if cat:
#        category = Category.query.filter_by(name=k).first()
#        category.links += list
#        db.session.commit()
#    else:
#        category = Category(name=k, links=list)
#        print(category)
#        db.session.add(category)
#        db.session.commit()


@app.route('/')
def index():
    return "Hello!"

# @app.route('/addCategory', methods=["POST"])
# def addCategory():
#     if request.form:
#         try:
#             category = Category(name=request.form.get("name"))
#             db.session.add(category)
#             db.session.commit()
#         except Exception as e:
#             print("Failed to add category")
#             print(e)
#     return 'Added!'


@app.route('/getCaregory', methods=['POST'])
def getCategory():
    data = request.json

    #return url
    link = Category.query.filter(Category.links.contains(data['url'])).first()

    if link:
        print('>>>>>>>')
        print(link.name)
        return link.name


if __name__ == "__main__":
    app.run(debug=True)